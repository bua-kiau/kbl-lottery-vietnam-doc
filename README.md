# KBL遊戲系統-越南彩-單一錢包接入文件
  
# 文件規範
  - 使用TPC/IP作為傳輸層協議、使用HTTP作為應用層協議
  - 使用token交換玩家資訊與處理餘額變動
  - 為維護資訊安全,平台方產生之token需自行實現驗證與失效需求

# 接口說明
## KBL系統接口清單
  - login-game(GET)<b>-創建遊戲</b>

## 平台方接口清單
  - user-info(GET)-<b>玩家資訊</b>
  - bet(POST)-<b>下注接口</b>
  - payout(POST)-<b>支付彩金</b>


# login-game(GET)-登入遊戲
### Reuqest

- Method: **GET**
- URL: ```/kbl-lottery-vietnam/api/v1/login-game```
- Headers： Content-Type:application/x-www-form-urlencoded
- QueryString:
```
  platform_id={由KBL系統統一發發放}&token={平台自定義}&lobby_url={遊戲內返回連結,須包含protocol之完整路徑,目前支援http與https協議}
```

### Response
- Body
```
{ 
  頁面跳轉
}
```


# user-info(GET)-玩家資訊
### Reuqest

- Method: **GET**
- URL: ```{由平台方定義}```
- Headers： Content-Type:application/x-www-form-urlencoded;Authorization:{平台方登入時給予的token}
- QueryString:
```
  return_type={有下列枚舉值:full(返回所有欄位)、balance(僅返回user_balance),如平台不處理此條件請一律返回所有欄位}
```

### Response
- Body
```
{ 
	"ret_code": String(6){調用狀態,"000000"為成功返回資訊,其餘情況視為失敗}}
	"ret_data": {
		"user": {
			"user_account": String(32){於平台方註冊的玩家帳號,查詢注單記錄時的定位}},
			"user_name": String(32){前端呈現的玩家暱稱},
			"user_image": String(256){傳入玩家頭像,需以包含protocol之完整網路資源路徑定義,支援http與https協議},
			"user_balance": Integer(11){玩家餘額,單位為分}
		}
	}
}
```


# bet(POST)-創建注單
### Reuqest

- Method: **POST**
- URL: ```{由平台方定義}```
- Headers： Content-Type:application/json;Authorization:{平台方登入時給予的token}
- Body:
```
{  
	"bet_id": Integer(11){注單唯一識別,保證唯一性,可用來校驗操作是否已處理},
	"bet_amt": Integer(11){下注金額,單位為分}
}
```

### Response
- Body
```
{
	"retCode": String(6){調用狀態,"000000"為成功扣款,其餘情況注單將不成立}}
	"ret_data": {
		"user": {
			"user_balance": Integer(11){操作後玩家餘額,單位為分}
		}
	}
}
```


## payout(POST)-支付彩金
### Reuqest

- Method: **POST**
- URL: ```{由平台方定義}```
- Headers： Content-Type:application/json;Authorization:{平台方登入時給予的token}
- Body:
```
{  
	"payout_id": Integer(11){返彩單唯一識別,保證唯一性,可用來校驗操作是否已處理}
	"payout_amt": Integer(11){須返還玩家之彩金(包含注金與獎金),單位為分}
}
```

### Response
- Body
```
{
	"retCode": String(6){調用狀態,"000000"為成功處理,其餘情況派彩單將不更新並重試通知}}
	"ret_data": {
		"user": {
			"user_balance": Integer(11){操作後玩家餘額,單位為分}
		}
	}
}
```
